//
//  AppDelegate.swift
//  HackerBooks
//
//  Created by Begoña Hormaechea on 27/1/17.
//  Copyright © 2017 begohorma. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
       
        do{
            
            // Cargar datos del modelo de su fuente
           let booksAr:BookArray = try loadDataFromSource()
            
            //Crear modelo Library
            let model = Library(books: booksAr)
        
            //Controladores
            
            //Crear el LibraryVC
            let libraryVC = LibraryTableViewController(model: model)
            
            //Crear el BookVC
            //Por defecto se pasa el primer libro del primer tag
            let firstTag: String = (model.libraryTags.first?.tagName)!
            //por defecto el primer libro de la primera temática.
            //TODO - guardar último libro visto en ¿userDefaults?
            let bookVC = BookViewController(model: model.book(forTagName: firstTag, at: 0)!)
            
            
            //Combinadores
            //Nav para la library
            let libraryNav = UINavigationController(rootViewController: libraryVC)
            //Nav para Book
            let bookNav = UINavigationController(rootViewController: bookVC)
            
            //window
            //hacer que ocupe toda la pantalla
            window = UIWindow(frame: UIScreen.main.bounds)
            
            
            // Identificar dispositivo y asignar los delgados y controlador principal apropiados para cada caso
            
            if UIDevice.current.userInterfaceIdiom == .phone{
                
                //cambio vista detalle al cambiar libro seleccionado
                libraryVC.delegate = bookVC
                
                window?.rootViewController = libraryNav
                
            }else{
                //ipad
                
                //SplitView - Para iPad
                let  splitVC = UISplitViewController()
                splitVC.viewControllers = [libraryNav,bookNav]
                splitVC.preferredDisplayMode = .allVisible
                
                
                // DELEGADOS
                //la lTVC será delegato del BookVC para enterearse de los fav
                bookVC.delegate = libraryVC
                //El BookVc será el delegado de LibraryVC
                libraryVC.delegate = bookVC
                
                //asignar VC principal
                window?.rootViewController = splitVC
            }
            
            
            //mostar window
            window?.makeKeyAndVisible()
            
           
            
        }catch{
            print("Error loading data from source")
        }
         return true
}


    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

