//
//  BookTag.swift
//  HackerBooks
//
//  Created by Begoña Hormaechea on 28/1/17.
//  Copyright © 2017 begohorma. All rights reserved.
//

import Foundation

class BookTag{
    
    // MARK: -  Properties
    let tagName : String
 
    
    // MARK: -  Initialization
    init(tagName: String ){
        //El nombre de los tags tendra la primera letra en mayusculas y las demás en minúsculas
        self.tagName = tagName.capitalized
    }
    
    

    
}


// MARK: -  Protocols

// Para poder usar los tags en el multidiccionario tienen que implementar el protocolo Hashable
//Como las tags son strings no hace falta un proxy. Se pueden comparar los nombres.

extension BookTag: Hashable{
    public var hashValue: Int
    {
        return tagName.hashValue
    }
}

extension BookTag: Equatable{
    public static func ==(lhs: BookTag, rhs: BookTag) -> Bool
        {
            return (lhs.tagName == rhs.tagName)
        }
    }

//Las etiquetas van ordenadas por favorito y luego orden alfabetico

//TODO - función que devuelva si el nombre del tag es Favorite
extension BookTag: Comparable{
    public static func <(lhs: BookTag, rhs: BookTag) -> Bool{
        if(lhs.tagName == "Favorite"){
            //Si el primero es el favorito es menor
            return true
        }else if(rhs.tagName == "Favorite"){
            //Si el segundo es el favorito no es menor
            return false
        }else{
            //si ninguno es favorito. orden alfabetico de nombre
            return lhs.tagName < rhs.tagName
        }
        
        
        
    }
}

