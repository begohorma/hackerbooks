//
//  Library.swift
//  HackerBooks
//
//  Created by Begoña Hormaechea on 28/1/17.
//  Copyright © 2017 begohorma. All rights reserved.
//

import Foundation
import UIKit



class Library{
    
    // MARK: -  Aliases
    //Usar el mutlidiccionario. La clave son los tags y los valores los libros
    typealias Books = MultiDictionary<BookTag,Book>
    typealias TagName = String
    
    // MARK: -  Properties
    
    var libraryBooks: Books
   
    var favoriteTag: BookTag = BookTag(tagName: "Favorite")
    
    
    
    
    // MARK: -  Initialization
    
    init(books bks: BookArray ){
        self.libraryBooks = Books() //multidiccionario vacio
      
        //Cargar los datos de los libros en el multidiccionario
        //La clave es un tag y el valor es el libro.
        //Como un libro puede tener más de un tag hay que recorrerlos y llamar al insert del multidic.
       
       
        //Array de los hasValue de los libros guardados en favoritos.
        //Se guardará en userDefaults
        let defaults = UserDefaults.standard
        let favoriteBooks:[Int] = defaults.array(forKey:"FavoriteBooks") as? [Int] ?? [Int]()
        

        //recorrer array libros
        for  book in bks{
            //comprobar si el hashValue del libro está en el userdefaults
            if favoriteBooks.contains(book.hashValue){
                //poner la propiedad favorite a true y añadir el tag favorite al libro
                book.favorite = true
                libraryBooks.insert(value: book, forKey: favoriteTag)
            }
            //recorrer le array de tags del libro
            for tag in book.tags{
                 libraryBooks.insert(value: book, forKey: tag)
            }
        }
        
        //
    }
    

    
    //Numero total de libros
    var booksCount: Int{
    get{
        return libraryBooks.countUnique
        }
    }
    
    //Numero de tags
    var libraryTags :[BookTag]{
        get{
           return [BookTag](libraryBooks.keys).sorted()
        }
    }
    
    //Cantidad de libros de una temática
    //Si no exite el tag devolver 0
    
    func bookCount(forTagName name: TagName) -> Int{
        //hay que convertir el string TagName en un BookTag
        //que es el tipo de la clave del multiDic.
        let tag = BookTag(tagName: name)
        
        //comprobar si existe la clave
        if let k = libraryBooks[tag]{
            return k.count //se devuelve el count para esa clave
        }
        else{
            //si no existe la clave
            return 0
        }
        
       
    }
    
    //Array de libros de una temática
    //Si no hay libros para una tematica se devuelve nil
    func books(forTagName name: TagName) -> [Book]?{
        let tag = BookTag(tagName: name)
        
        //comprobar que exista la clave
        guard let k = self.libraryBooks[tag] else{
            return nil
        }
        // TDODO ??? - ¿El set de la clave puede estar vacio???
        return k.sorted()

    }
    
    //Libro que está en una determindada posición dentro de un Tag.
    //Si el el idice no exite se devuelve nil
    func book(forTagName name: TagName, at: Int) -> Book?{
        //obtener el set de libros para la clave
        guard let bks = books(forTagName: name)else{
            return nil
        }
        //comprobar que la posicion está dentro de rango:
        //tiene que haber algun libro y el la posición <= que total libros
        guard (libraryBooks.count > 0 && at <= libraryBooks.count) else {
            return nil
        }
        
//        guard (at <= libraryBooks.count) else{
//            return nil
//        }
        //devolver el libro de la posición at
        return bks[at]
        
    }
    
   
  // MARK: -  Favorites
    
    // Necesito añadir un libro a favoriotos y poder quitar un libro de favoritos
    func addBookToFavorites(book : Book)
    {
        //Guardar en el multidic.
        libraryBooks.insert(value: book, forKey: favoriteTag)
        //Guardar libro en favoritos de UserDefaults
        addFavoriteBookToUserDefaults(book: book)
    }
    
    func removeBookToFavorite(book : Book)
    {
        libraryBooks.remove(value: book, fromKey: favoriteTag)
        //Eliminar libro de favoriots de UserDefaults
        removeFavoriteBookFromUserDefaults(book: book)
           }
    
    
    
    // MARK: -  UserDefults
    func addFavoriteBookToUserDefaults(book: Book){
        let defaults = UserDefaults.standard
        var favoriteBooks:[Int] = defaults.array(forKey:"FavoriteBooks") as? [Int] ?? [Int]()
        favoriteBooks.append(book.hashValue)
        defaults.set(favoriteBooks, forKey:"FavoriteBooks")
    }
    
    func removeFavoriteBookFromUserDefaults(book: Book){
        let defaults = UserDefaults.standard
        var favoriteBooks:[Int] = defaults.array(forKey:"FavoriteBooks") as? [Int] ?? [Int]()
        //comprobar que existe el book.hasValue en el favoriteBooks
        guard let index = favoriteBooks.index(of:book.hashValue)
            else{return}
        //eliminar de favoritos si existe
        favoriteBooks.remove(at: index)
        
        //favoriteBooks.remove(at: favoriteBooks.index(of: book.hashValue)!)
        defaults.set(favoriteBooks, forKey:"FavoriteBooks")
        
    }
    

    
    
    
}
