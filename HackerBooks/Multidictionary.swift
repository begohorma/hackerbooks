//
//  Multidictionary.swift
//  HackerBooks
//
//  Created by Begoña Hormaechea on 31/1/17.
//  Copyright © 2017 begohorma. All rights reserved.
//

import Foundation



//Value: lo que va dentro de un Set-> bucket (book)
//Key: la clave (tag)
//Tienen que implementar hashable para que se puedan comparar y ordenar
public
struct MultiDictionary <Key: Hashable, Value: Hashable>{
    
    // MARK: -  Types
    public
    typealias Bucket = Set<Value> //El Set (conjunto) se le llama cubo
    
    // MARK: -  Properties
    private
    var _dict: [Key: Bucket] //diccionario con una clave asociada a un cubo
    
    // MARK: -  Lifecycle
    
    //Crear diccionario vacio
    public
    init(){
        _dict = Dictionary()
    }
    
    // MARK: -  Accessors
    
    public
    var isEmpty: Bool {
        return _dict.isEmpty
    }
    
    //Número de cubos -> Lo mismo que cuantas claves -> (secciones de la tabla (tags))
    public
    var countBuckets: Int {
        //pregunar al diccionario cuantos objetos tiene
        return _dict.count
    }
    
    //   Cuantos objetos hay en total -> cuantos libros hay en todas las secciones.
    //Un libro en dos secciones cuenta dos veces
    
    public
    var count : Int{
        //Recorrer todos los cubos y contar los elementos de cada uno
    var total = 0
     for bucket in _dict.values{
            total += bucket.count
        }
    return total
    }
    
    // cuantos objetos únicos hay en total
    public
    var countUnique: Int{
        var total = Bucket()
        //El Set ignora los repetidos. Se mete el contenido de los buckets en otro que ignorará los repetidos
        //El count del nuevo bucket será el total de elementos únicos
        for bucket in _dict.values{
            total = total.union(bucket)
        }
        return total.count
    }
    
    //Añadido
    public
    var keys : LazyMapCollection<Dictionary<Key, Bucket>,Key> {
        return _dict.keys
    }
    
    public
    var buckets : LazyMapCollection<Dictionary<Key, Bucket>,Bucket> {
        return _dict.values
    }
    
    // MARK: -  Setters (Mutators)
    //Modifican contenido
    
    public
    subscript(key: Key) -> Bucket?{
        //Si no hay nada para la clave indicada devolverá nil.
        get{
            return _dict[key]
        }
        
        set(maybeNewBucket){
            //comprobar si es nil
            guard let newBucket = maybeNewBucket else{
                return //salir
            }
            //comprobar si ya existia la clave.
            guard let previous = _dict[key] else {
                // Si no hay nada bajo la clave se añade el bucket
                _dict[key] = newBucket
                return
            }
            
            //Crear la union de lo viejo y lo nuevo
             _dict[key] = previous.union(newBucket)
            
        }
    }
    
    //Insertar un valor para una clave (Un nuevo libro para un tag)
    //Toda función que cambie el estado de una struct tiene que llevar "mutating"
    
    public
    mutating func insert(value : Value, forKey key: Key){
        
        //si existe la clave se le añade un nuevo valor
        if var previous = _dict[key]{
            previous.insert(value)
            _dict[key] = previous
        }else{
            //si no existe  se crea un bucket
            _dict[key] = [value]
        }
    }
    
    
    //Eliminar un valor de un bucket. Quitar un libro de un tag (quitarle el favorito)
    
    public
    mutating func remove(value: Value, fromKey key: Key){
        
        //comprobar si existe un bucket para la clave
        guard var bucket = _dict[key] else {
            return
        }
        
        //comprobar que el bucket tiene el valor
        guard bucket.contains(value) else {
            return
        }
        
        bucket.remove(value)
        
        if bucket.isEmpty{
            //Si al borrar el valor el bucket se queda vacio hay que elimnar la clave del diccionario
            _dict.removeValue(forKey: key)
        }else{
            _dict[key] = bucket
        }
    }
    
    
}
