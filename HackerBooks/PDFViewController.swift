//
//  PDFViewController.swift
//  HackerBooks
//
//  Created by Begoña Hormaechea on 28/1/17.
//  Copyright © 2017 begohorma. All rights reserved.
//

import UIKit

class PDFViewController: UIViewController {
    
    
    // MARK: -   Properties
    
    var model: Book

    // MARK: -  Outlets
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var browser: UIWebView!
    
    
    // MARK: -  Initialization
    
    init(model: Book){
        self.model = model
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: - Sync model -> View
    
    func syncViewWithModel() {
        
        //Descargar el pdf -
        //Va a bloquear habrá que cambiarlo a segundo plano cuando lo veamos -> AsyncData
        let pdfName :String = model.pdfUrl.lastPathComponent
        var pdfData: Data? = nil
        do{
            
            pdfData = try? loadPdf(fileName: pdfName, originUrl: model.pdfUrl)
            
            //cargar el data
            browser.load(pdfData!, mimeType: "application/pdf", textEncodingName: "utf8", baseURL: model.pdfUrl)
            
        }catch{
            print("Error cargando pdf")
        }
        
    }
    
    
    // MARK: -  view lifeCycle
    
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // hay que indicarle al browser que soy su delegado para que avise de cuando se carga
        browser.delegate = self
        syncViewWithModel()
        
        self.edgesForExtendedLayout = []
        
        // suscribirse a notificaciones
        subscribe()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // darse de baja de las notificaciones
        unsubscribe()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

// MARK: -  Notifications
extension PDFViewController{
    func subscribe(){
        //Hacer referencia al NotificationCenter
        let nc = NotificationCenter.default
        
        //suscribirse a notificación en concreto y determinar acciones a realizar cuando se produce la notificación
        //object: es el remitente. Ni idea: nil
        //OperationQueue.main: cola principal. Ejecución en primer plano.
        
        nc.addObserver(forName: LibraryTableViewController.notificationName, object: nil, queue: OperationQueue.main) { (note:Notification) in
            
            //Extraer libro de la notificación
            let userInfo = note.userInfo
            let book = userInfo?[LibraryTableViewController.bookKey]
            
            // cambiar de libro
            //como del userInfo puede ser un opcinal hay que indicar que es un Book
            self.model = book as! Book
            
            //actualizar la vista.
            self.syncViewWithModel()
        }
        
    }
    
    func unsubscribe(){
        //Hacer referencia al NotificationCenter
        let nc = NotificationCenter.default
        //darse de baja de las notificaciones existentes
        nc.removeObserver(self)
    }
}




// Se añade indicador de actividad para indicar que el pdf se está cargando
//sobre todo cuando hay que descargar el pdf por primera vez que tardará
// Esto NO SIRVE para indicar el tiempo de descarga porque descargo antes de llamar a load así que no me
//puede avisar - ¿Necesito notificaciones de cuando se pulsa el botón o ?

// MARK: -  UIWebViewDelegate
extension PDFViewController: UIWebViewDelegate{
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        // Mostrar el spinner y activarlo
        spinner.isHidden = false
        spinner.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        //Ocultar el spinner y pararlo
        spinner.isHidden = true
        spinner.stopAnimating()
    }
}
