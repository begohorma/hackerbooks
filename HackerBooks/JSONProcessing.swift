//
//  JSONProcessing.swift
//  HackerBooks
//
//  Created by Begoña Hormaechea on 28/1/17.
//  Copyright © 2017 begohorma. All rights reserved.
//

import Foundation
import UIKit

// MARK: -  Aliases
typealias JSONObject = AnyObject
typealias JSONDictionary = [String : JSONObject]
typealias JSONArray = [JSONDictionary]

typealias BookArray = [Book]


// MARK: -  constantes?

/// Name with which JSON file will be saved in Documents
let fileName:String = "books_readable.json"

///Url from JSON file will be downloaded
let fileUrl:String = "https://t.co/K9ziV0z3SJ"

///Url to Documents Directory
let documentsUrl:URL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL!



//MARK: - Loading

func loadDataFromSource() throws -> BookArray{
    
    var booksAr: BookArray = []
    
    //TODO: usar NSUserDefaul con un clave o comprobar si exite el documento
    //decidir que hacer
    
    //comprobar si el fichero existe en Documents
    if fileExistsInDocuments(fileName: fileName){
        //cargar el fichero de documents
       booksAr = try loadDataFromDocuments(fileName: fileName)
    }else{
        //descargar el fichero a documents
        booksAr = try loadDataFromUrl(urlString: fileUrl)
    }
    
    return booksAr
}


func loadDataFromUrl(urlString url:String) throws -> BookArray{
   
    var booksAr: BookArray
    
    //crear una url a partir del string y comprobar que es correcta
    guard let url = URL(string:url)else{
        throw HackerBookError.wrongURL
    }
    
    //obtener los datos de la url usando el inicializador de Data
    guard let data = try? Data(contentsOf: url)else{
        throw HackerBookError.downloadingFile
    }
    
    
    //guardar los datos en Documents
       //Definir URL destino en documents
    let destinationFileUrl = documentsUrl.appendingPathComponent(fileName)
      //escribir data en la url en Documents
    try data.write(to: destinationFileUrl)
    
    
    //obtener el arrayJson del data
    //el json ya está en documents. Utilizar func de carga desde documents
    booksAr = try loadDataFromDocuments(fileName: fileName)
    
    return booksAr
    
    
}

func loadDataFromDocuments(fileName name:String)  throws -> BookArray{
   var booksAr: BookArray
    
    //obtener el arrayJson del data
    
    guard let jsonAr:JSONArray = try? loadFromDocumentsFile(fileName: fileName)
        else{
            throw HackerBookError.jsonParsingError
    }
    
    //parsear json para convertirlo en array de books
    do{
        booksAr = try decode(books: jsonAr)
        
    }catch{
        throw HackerBookError.errorDecodingBooks
    }


    return booksAr
}



/**
 Load a Json File from documents and return an Array of dictionaries of JSON Objects
*/
func loadFromDocumentsFile(fileName name: String) throws -> JSONArray{
    
    //Definir URL destino en documents
    //let documentsUrl:URL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL!
    let destinationFileUrl = documentsUrl.appendingPathComponent(name)
    
    
    if let data = try? Data(contentsOf: destinationFileUrl),
        let maybeArray = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? JSONArray,
        let array = maybeArray{
        
        return array
    }else{
        throw HackerBookError.loadingFile
    }
}


//TODO - La carga de photos y pdfs son muy parecidas. Se podrían hacer con métodos que usen genericos
//y valgan para todo tipo de ficheros

// MARK: -  photos
func loadCover(fileName name: String, originUrl url: URL) throws-> UIImage{
    
    var image: UIImage
    //let photoName:String = URL(fileURLWithPath: name).lastPathComponent
    
    //comprobar si el fichero de la foto existe en Documents
    if fileExistsInDocuments(fileName: name){
        //cargar la imagen de documents
        image = try loadCoverFromDocuments(coverName: name)
        
    }else{
        //descargar el fichero a documents
        image = try loadCoverFromUrl(coverUrlString: url, fileName: name)
    }

    return image
}

func loadCoverFromUrl(coverUrlString url: URL, fileName name: String) throws  -> UIImage{
    
    var image: UIImage
    

    //obtener los datos de la url usando el inicializador de Data
    guard let data = try? Data(contentsOf: url)else{
        throw HackerBookError.downloadingFile
    }
    //guardar los datos en Documents
    //Definir URL destino en documents
    let destinationFileUrl = documentsUrl.appendingPathComponent(name)
    //escribir data en la url en Documents
    try data.write(to: destinationFileUrl)
    
    // obtener la imagen de documents
    image =  try loadCoverFromDocuments(coverName: name)
    
    return image
}

func loadCoverFromDocuments(coverName name: String) throws -> UIImage{
    
    let destinationFileUrl = documentsUrl.appendingPathComponent(name)

    if let image = UIImage(data: try Data(contentsOf: destinationFileUrl)){
        return image
    }else{
        throw HackerBookError.loadingFile
    }
    
    
}


// MARK: -  pdf
func loadPdf(fileName name: String, originUrl url: URL) throws-> Data{
    
    var pdfData: Data

    
    //comprobar si el fichero del pdf existe en Documents
    if fileExistsInDocuments(fileName: name){
        //cargar el pdf de documents
        pdfData = try loadPdfFromDocuments(pdfName: name)
        
    }else{
        //descargar el fichero a documents
        pdfData = try loadPdfFromUrl(pdfUrlString: url, fileName: name)
    }
    
    return pdfData
}

func loadPdfFromUrl(pdfUrlString url: URL, fileName name: String) throws  -> Data{
    
    var pdfData: Data
    
    
    //obtener los datos de la url usando el inicializador de Data
    guard let data = try? Data(contentsOf: url)else{
        throw HackerBookError.downloadingFile
    }
    //guardar los datos en Documents
    //Definir URL destino en documents
    let destinationFileUrl = documentsUrl.appendingPathComponent(name)
    //escribir data en la url en Documents
    try data.write(to: destinationFileUrl)
    
    // obtener el pdf de documents
    pdfData =  try loadPdfFromDocuments(pdfName: name)
    
    return pdfData
}

func loadPdfFromDocuments(pdfName name: String) throws -> Data{
    

    let destinationFileUrl = documentsUrl.appendingPathComponent(name)
    
    if let pdfData = try? Data(contentsOf: destinationFileUrl){
        return pdfData
    }else{
        throw HackerBookError.loadingFile
    }
    
    
}


// MARK: -  Files
//TODO - Probar a documentar las funcionescon los tags
/**
 Check if a given file exists in Documents directory
 
 :return: Bool True if the file exists, False if file doesn't exits
 */
func fileExistsInDocuments(fileName name:String) -> Bool{
    
    var filePath = ""
    
    //ruta a Documents
    let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
    
    //Comprobar si Documents tiene algo
    if documentsPath.count > 0 {
        let dir = documentsPath[0]
        filePath = dir.appendingFormat("/" + name)
    }
    
    let fileManager = FileManager.default
    if fileManager.fileExists(atPath: filePath){
//        print("File exists.")
        return true
        
    }else{
        //El fichero no existe en documents. Descargarlo
        return false
    }
    
}



// MARK: -  Decodification


//    "authors": "Scott Chacon, Ben Straub",
//    "image_url": "http://hackershelf.com/media/cache/b4/24/b42409de128aa7f1c9abbbfa549914de.jpg",
//    "pdf_url": "https://progit2.s3.amazonaws.com/en/2015-03-06-439c2/progit-en.376.pdf",
//    "tags": "version control, git",
//    "title": "Pro Git"
/**
 Transforms a Dictionary of JSONObjects relating to a book into a Book object
*/
func decode(book json: JSONDictionary) throws -> Book{
  
   // Validar el diccionario
    
    guard let photoUrlString = json["image_url"] as?  String,
        //si era una cadena y tiene formato de url se guardará en url
        //si no tiene formato de cadena devolverá nil
        let photoUrl = URL(string: photoUrlString)
        else {
            
            throw HackerBookError.wrongURLFormatForJSONResource
    }
    
    guard let pdfUrlString = json["pdf_url"] as?  String,
        //si era una cadena y tiene formato de url se guardará en url
        //si no tiene formato de cadena devolverá nil
        let pdfUrl = URL(string: pdfUrlString)
        else {
            
            throw HackerBookError.wrongURLFormatForJSONResource
    }
    
    guard let title = json["title"] as? String else{
                   throw HackerBookError.wrongURLFormatForJSONResource
                }
    
    //TODO -  Intentar resolver los warning
    
    //authors y tags son strings
    guard let authorsString = json["authors"] as? String,
    let authors = authorsString.components(separatedBy: ",") as? Authors else{
        throw HackerBookError.wrongURLFormatForJSONResource
    }
    
    guard let tagsString = json["tags"] as? String,        //array de strig con nombres de tgas
      //convertir el array de strings en un array de BookTag
        let tags: [BookTag] = parseTags(tagsArray:tagsString.components(separatedBy: ","))
    else{
        throw HackerBookError.wrongURLFormatForJSONResource
    }

    
    //despues de comprobar
    
    
    return Book(title: title, authors: authors, tags: tags, photoUrl: photoUrl, pdfUrl: pdfUrl)
}


func parseTags(tagsArray: [String]) -> [BookTag]{
    
    var bTags: [BookTag]=[]
    for t in tagsArray{
        bTags.append(BookTag(tagName: t))
    }
    
    return bTags
        
}
/**
 Transforms an Array of Dictionaries of JSONObjects relating to books into an array of Book objects
 */
func decode(books json:JSONArray) throws -> BookArray{
    
    var bArray : BookArray = []
    
    do{
        bArray = try json.flatMap({
            try decode(book: $0)
        })
    }catch{
       throw HackerBookError.errorDecodingBooks
    }
    return bArray
}












