//
//  Book.swift
//  HackerBooks
//
//  Created by Begoña Hormaechea on 28/1/17.
//  Copyright © 2017 begohorma. All rights reserved.
//

import Foundation
import UIKit
typealias Authors = [String]
typealias Tags = [BookTag]

class Book{
    
    
//    "authors": "Scott Chacon, Ben Straub",
//    "image_url": "http://hackershelf.com/media/cache/b4/24/b42409de128aa7f1c9abbbfa549914de.jpg",
//    "pdf_url": "https://progit2.s3.amazonaws.com/en/2015-03-06-439c2/progit-en.376.pdf",
//    "tags": "version control, git",
//    "title": "Pro Git"
    
    // MARK: -  Aliases
    
    //typealias Tags = [String]
    
    // MARK: -  Stored properties
    let title: String
    let authors: Authors
    let tags: Tags
    var photo: UIImage = UIImage()
    let photoUrl: URL
    let pdfUrl: URL
    var favorite : Bool = false //propiedad que indica si el libro es favorito o no.Es modificable
    
    
    
    // MARK: -  Initialization
    init(title: String,
         authors: Authors,
         tags: Tags,
         photoUrl: URL,
         pdfUrl: URL){
        
        self.title = title
        self.authors = authors
        self.tags = tags
        self.photoUrl = photoUrl
        self.pdfUrl = pdfUrl
        
        //Descargar la photo -
            //Va a bloquear habrá que cambiarlo a segundo plano cuando lo veamos -> AsyncData
        let photoName :String = photoUrl.lastPathComponent
        do{
        self.photo = try loadCover(fileName: photoName, originUrl: photoUrl)
            
        }catch{
            print("Error cargando cover")
        }
        
    }
   
    // MARK: -  Favorites
    
    var isFavorite :Bool{
        get{
            return favorite
        }
    }
    
    func switchFavorite(){
        //cambiar al valor contrario al actual
        self.favorite = !self.favorite
    }

    
    
    
    // Para poder usar el multidiccionario book tiene que ser hashable.
    // Para identificar a un libro uso un proxy para generar una cadena que lo identifique  y se puede usar para implementar los protocolos
    //Equatable y Comparable.
    
    
    // MARK: -  Proxies
    func proxyForEquality() -> String{
        // cadena que representa a un book
        return "\(title)\(authors)"
    }
    
    func proxyForComparison() -> String{
        //de momento igual que igualdad. ¿tengo que cambiarlo?
        // cadena que representa a un book
        return "\(title)\(authors)"
    }

    
}

 // MARK: -  Protocols

// para poder ver una descripción del libro

extension Book: CustomStringConvertible{
    public var description : String {
        get{
            return "<\(type(of:self)) : \(title) de \(authors)>"
        }
    }
}


extension Book: Equatable{
    
    public static func ==(lhs:Book, rhs: Book) -> Bool{
        return(lhs.proxyForEquality() == rhs.proxyForEquality())
    }
    
}
extension Book: Hashable{
    public var hashValue: Int {
        get{
            return self.proxyForEquality().hashValue
        }
    }
}

//TODO- La comparación va a ser igual que la igualdad??? Hacer un proxy para comparar por si es distinto

extension Book : Comparable{
    public static func <(lhs: Book, rhs: Book) -> Bool{
        return(lhs.proxyForComparison() < rhs.proxyForComparison())
    }
}




