//
//  LibraryTableViewController.swift
//  HackerBooks
//
//  Created by Begoña Hormaechea on 28/1/17.
//  Copyright © 2017 begohorma. All rights reserved.
//

import UIKit

class LibraryTableViewController: UITableViewController {
    
    
    // MARK: -  Constants
    
    static let notificationName = Notification.Name(rawValue: "BookDidChange")
    static let bookKey = "BookKey"
    
    
    // MARK: -  Properties
    let model: Library
    
    //definir delegado
   weak var delegate : LibraryTableViewControllerDelegate? = nil
    
    
    // MARK: -  Initialization
    
    init(model: Library){
        self.model = model
        super.init(nibName: nil, bundle: nil)
        self.title = "Hacker Books"
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        subscribe()
    }

    override func viewWillDisappear(_ animated: Bool) {
        unsubscribe()
    }
    
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        //Numero de tags.
        return model.libraryTags.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //Numero de libros de la sección indicada
        //hay que convetir la sección en un nombre de tag para poder llamar a la función que devuelve el 
        //el númeto de libros de un Tag
       let tagName = model.libraryTags[section].tagName
        
        return model.bookCount(forTagName: tagName)

    }
    
    //Poner nombre a las secciones
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
       return  model.libraryTags[section].tagName
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //Definir id para el tipo de celda
        let cellId = "HackerBookCell"
        
        //Averiguar la seccion (tag)
        let tag = model.libraryTags[indexPath.section]
        //Averiguar cual es el libro
        let book = model.book(forTagName: tag.tagName, at: indexPath.row)
        
        //Crear la celda
        var cell = tableView.dequeueReusableCell(withIdentifier: cellId)
        
        //Comprobar si hay celda
        if cell == nil{
            //si no hay celda hay que crear una
          cell = UITableViewCell(style: .subtitle, reuseIdentifier: cellId)
        }
        
        //configurar la celda
        cell?.imageView?.image = book?.photo
        cell?.textLabel?.text = book?.title
        cell?.detailTextLabel?.text = book?.authors.joined(separator: ",")
        
        return cell!
    }

    
    // MARK: -   Table View Delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // identificar el libro
        //Averiguar la seccion (tag)
        let tag = model.libraryTags[indexPath.section]
        //Averiguar cual es el libro
        let book = model.book(forTagName: tag.tagName, at: indexPath.row)!
        
        if UIDevice.current.userInterfaceIdiom == .phone{
            // crear el BookVC
            let bookVC = BookViewController(model:book)
            
            //pushearlo
            navigationController?.pushViewController(bookVC, animated: true)
        }else{
            //Con splitVC es necesario tener bookVC como delegado
            //Avisar al delegado
            delegate?.libraryTableViewController(self, didSelectBook: book)
            
            // mandar notificación
            notify(bookChanged: book)
        }
       
        
       
        
    }
    
// TODO - Celda personalizada
    
}

// MARK: -  Delegate Protocol
protocol LibraryTableViewControllerDelegate : class{
    
    func libraryTableViewController(_ lTVC:LibraryTableViewController, didSelectBook book: Book )
    
}


// MARK: -  Notifications
extension LibraryTableViewController{
    func notify(bookChanged book: Book){
        
        //Crear o solicitar instancia del NotificationCenter
        let nc = NotificationCenter.default
        
        //Crear objeto notificación
        let notification = Notification(name: LibraryTableViewController.notificationName, object: self, userInfo: [LibraryTableViewController.bookKey: book])
        
        //mandarla
        nc.post(notification)
    }
}

// MARK: -  Notifications


extension LibraryTableViewController{
    func subscribe(){
        //Hacer referencia al NotificationCenter
        let nc = NotificationCenter.default
        nc.addObserver(forName: BookViewController.notificationName, object: nil, queue: OperationQueue.main){
            (note: Notification) in
            //Extraer libro de la notificación
            let userInfo = note.userInfo
            let book = userInfo?[LibraryTableViewController.bookKey] as! Book
            
            if book.isFavorite{
                self.model.addBookToFavorites(book: book)
            }else{
                self.model.removeBookToFavorite(book: book)
            }
            
            //sincronizar vista
            //recargar el tableView
            self.tableView.reloadData()
        
        }
    }
    
    
    func unsubscribe(){
        //Hacer referencia al NotificationCenter
        let nc = NotificationCenter.default
        //darse de baja de las notificaciones existentes
        nc.removeObserver(self)
    }
}



// MARK: -  Favorites

extension LibraryTableViewController: BookViewControllerDelegate{
    func bookViewController(_ bVC: BookViewController, didBookFavoriteStateChange book: Book) {
        
        // actualizar valor de favorito del modelo
        //identificar si el libro ahora es favorito o no
        //si es favortio añadirle al libro el tag favorite y sino quitarselo
        if book.isFavorite{
            model.addBookToFavorites(book: book)
        }else{
            model.removeBookToFavorite(book: book)
        }
        
        //sincronizar vista
        //recargar el tableView
        self.tableView.reloadData()
    }
}




