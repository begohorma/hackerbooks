//
//  Errors.swift
//  HackerBooks
//
//  Created by Begoña Hormaechea on 28/1/17.
//  Copyright © 2017 begohorma. All rights reserved.
//

import Foundation

//TODO - Revisar los que no estoy utilizando y si los nombres son correctos
enum HackerBookError : Error{
    case loadingDataFromSource
    case loadingFile
    case loadingFileFromDocuments
    case wrongURL
    case jsonParsingError
    case downloadingFile
    case errorDecodingBooks
    case resourcePointedByURLNotReachable
    case wrongURLFormatForJSONResource
}
