//
//  BookViewController.swift
//  HackerBooks
//
//  Created by Begoña Hormaechea on 28/1/17.
//  Copyright © 2017 begohorma. All rights reserved.
//

import UIKit

class BookViewController: UIViewController {
    
    
    // MARK: -  Constants
    static let notificationName = Notification.Name(rawValue: "BookFavoriteStatusDidChange")
    static let bookKey = "BookKey"
    
    
    // MARK: -  Properties
    
    var model: Book
    weak var delegate: BookViewControllerDelegate? = nil
    
    // MARK: -  Initialization
    
    init(model: Book){
        self.model = model
       
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: -  Outlets
    
    @IBOutlet weak var FavoriteImage: UIBarButtonItem!
    @IBOutlet weak var coverView: UIImageView!
       
    // MARK: -  Actions
   
    @IBAction func switchFavorite(_ sender: UIBarButtonItem) {
      //Cambiar de no favorito a favorito y viceversa.
        model.switchFavorite()
        
        //cambiar el icono
        //TODO -  método para cambiar iconos
        syncFavoriteImage()
        
        // avisar al delegado para que actualice 
//        delegate?.bookViewController(self, didBookFavoriteStateChange: model)
        
        
        if UIDevice.current.userInterfaceIdiom == .phone{
            //mandar notificación
            notify(bookFavoriteStateChaged: model)
//           navigationController?.popViewController(animated: true)
        }else{
            // avisar al delegado para que actualice
            delegate?.bookViewController(self, didBookFavoriteStateChange: model)
 
        }
        
        
    }
    
    @IBAction func displayPdf(_ sender: UIBarButtonItem) {
        
        //Crear PdfVC
        let pdfVC = PDFViewController(model: model)
        
        //Hacer push
        navigationController?.pushViewController(pdfVC, animated: true)
    }
    
    


    
    
// MARK: -  Sync model ->View
    func syncWithModel(){
       coverView.image = model.photo
       title = model.title
    }
    
    func syncFavoriteImage(){
        FavoriteImage.image = model.isFavorite ? UIImage(named: "Estrella Filled-50.png"): UIImage(named: "Estrella-50.png")
    }
// MARK: -  LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.edgesForExtendedLayout = []
        syncWithModel()
        syncFavoriteImage()
    }

}

// MARK: -  Protocols
//implementar el método de delegado de lTVC para actualizar modelo cuando se seleccione un nuevo libro
extension BookViewController: LibraryTableViewControllerDelegate{
    func libraryTableViewController(_ lTVC: LibraryTableViewController, didSelectBook book: Book) {
        //cambiar modelo
        model = book
        //sincronzar vista y modelo
        syncWithModel()
        syncFavoriteImage()
    }
}

// MARK: -  Delegate Protocol
//Definir el protocolo que tiene que implementar el delegado de BookVC para ser informado que un libro ha cambiado el estado de favorito

protocol BookViewControllerDelegate : class{
    
    func bookViewController(_ bVC:BookViewController, didBookFavoriteStateChange book: Book )
    
}

// MARK: -  Notifications
extension BookViewController{
    func notify(bookFavoriteStateChaged book:Book){
        
        //solicitar instancia notificationCenter
        let nc = NotificationCenter.default
        
        //Crear objeto notificación
        let notification = Notification(name: BookViewController.notificationName, object: self, userInfo: [BookViewController.bookKey: book])
        
        //mandarla
        nc.post(notification)
    }
}




