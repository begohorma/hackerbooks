# HackerBooks

## Modelo
### ¿Dónde guardarias las imágenes de las portadas y los pdfs?
- Dado que en esta versión de la aplicación no se dispone de base de datos y no estoy haciendo una descarga asíncrona de los documentos, he decidido guardarlos en documents para que se descarguen una única vez.

## Implementación favoritos.
 ### Persistencia de favoritos. Formas de hacerlo. Explicación de decisión de diseño elegida
 - Al no tener base de datos hay que seleccionar entre los otros mecanismos de persistencia disponibles:
    - Fichero json en la sandbox. Lo guardaría en documents porque en cache podría borrarse y no interesa perder las preferencias del usuario
    - NsUserDefults: para cantidades de datos pequeñas
    - NSCoding: para cantidades moderadas de datos
 - La lista de libros marcados como favoritos considero que es una cantidad de datos pequeña por lo que guardarlo en NsUserDefults me parece una buena opción
 - **PDTE IMPLEMENTAR**

### Actualización de la tabla al cambiar un favorito - Envio de información
  - En el caso de la versión para Ipad al estar visibles a la vez el LibraryTableViewController y el BookVC para avisar de que el libro ha cambiado el estado de su atributo favorito basta con que el LibraryTableViewController sea delegado del BookVC.
  - Al hacer la aplicación universal en el caso del iphone esto no funciona por lo que la mejor forma de transmitir la información son las notificaciones

### Uso del método *reloadData* para recargar la tabla
- En principio no me parece una aberración ya que por el funcionamiento del TableView sólo se carga en memoria lo que se está mostrando en pantalla.
- Supongo que una alternativa sería insertar o borrar la celda especifica del libro añadido o quitado de favoritos pero en principio me parece que complica mucho el código e implementar a mano funcionalidad que ya ofrece cocoa y seguramente está mucho más optimizada de lo que yo pueda hacer.
Necesitaría mirar más a fondo la docuemtación de los métodos del UITableView para precisar mejor esta respuesta.


 
## Controlador pdfs
 ### Actualización del PDFViewController cuando cambia en la tabla el libro seleccionado
 - Cuando se utiliza un SplitViewController es necesario avisar que se ha cambiado el libro seleccionado a dos controladores.
  Al BookVC se le avisa mediante el delegado. BookVC es delegado del SlplitView y es informado cuando se selecciona un nuevo libro para que actualice la vista de detalle del libro.
  EL PDFVC debería ser también delegado de SplitVC, pero como un objeto sólo puede tener un delegado es necesario utilizar las notificaciones para informar a más de un controlador.
  LibraryTableViewController envía una notificación cuando se selecciona un nuevo libro y PDFVC se suscribe a esa notificación para actualizar el PDF cuando la reciba.


### Mejoras pendientes de implementar (intentaré ir termnandolo hasta el Martes. ( Actualizaré el repo)
- Persistencia de los libros marcados como favoritos (NsUserDefults)
- Guardar último libro seleccionado para indicarlo como libro por defecto a mostrar en el bookVC.
- Hacer celda personalizada para la tabla de libros
- Mejora del icono de favoritos
- Uso de descarga asincrona utilizando la clase ASyncData proporcionada
- Parte extra de la práctica


